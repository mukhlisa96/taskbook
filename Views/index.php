<?php
$crud_object = new Crud();
?>
<!DOCTYPE html>
<html>
<head>
	<title>TASK BOOK APP</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
	.arrow{
		background-color: transparent;
		font-size:10px;
	}
</style>
<body>
	<div class="container box">
	<?php include './Views/auth.php';?>
		<h3 align="center">TASK BOOK</h3><br>
		<button type="button" name="add" class="btn btn-success" data-toggle="collapse" data-target="#task_form_collapse" >Add</button>
		<button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info " align="right">Login</button>
		<?php 
			$user_controller = new UserController();
				if (isset($_POST['login'])) {
					$username = $_POST['username'];
					$password = $_POST['password'];
					if ($user_controller->login($username, $password)) {
						?>
							<button type="submit" class="btn btn-warning" id="logout" name="logout" value="logout" >Logout</button>
							<h3  class="" id="logout" name="logout" value="logout" >hey admin</h3>
						<?php
					}else{
						echo "wrong user";
					}
				}
				if (isset($_POST['logout'])) {
							$user_controller->logout();
				}
			?>
		<br/>
		<br/>
		<div id="task_form_collapse" class="collapse">
			<form method="POST" enctype="multipart/form-data" id="task_form">
				<div class="">
			       <input type="text" name="employee" id="employee" class="form-control" placeholder="Enter User name " required/>
			       <br />
			       <input type="text" name="email" id="email" class="form-control" placeholder="Enter User email " required/>
			       <br />
			       <input type="text" name="taskcontent" id="taskcontent" class="form-control" placeholder="Enter  task content" required/>
			       <br />
			       <input type="file" name="taskimage" id="taskimage" required/>
			       <span id="taskimage"></span>
				   <input type="hidden" name="hidden_task_image" id="hidden_task_image">
				   <span id="uploaded_image"></span>			    
				 </div>
			     <div class="modal-footer">
			       <input type="hidden" name="action" id="action"/>
			       <input type="hidden" name="task_id" id="task_id">
			       <input type="submit" name="button_action" id="button_action" class="btn btn-primary" value="Preview" />
			     </div>
			</form>		
		</div>
		<div id="task_table" class="table-responsive">	
		</div>
	</div>



</body>
</html>


<?php
 if (isset($_SESSION['user'])) {
        $isadmin = 1;
      }else{
        $isadmin = 0;

      }
	if (isset($_POST['page'])) {
        $page = $_POST["page"];
      }
      else{
      	$page = 0;
      }
if (isset($_POST['orderBy'])) {
        $orderBy = $_POST["orderBy"];
      }
      else{
      	$orderBy = "ID";
      }?>

<script type="text/javascript">
	$(document).ready(function(){

		var isadmin ='<?php echo $isadmin ;?>';
		var page ='<?php echo $page ;?>';
		var orderBy ='<?php echo $orderBy ;?>';

		load_data(isadmin,page,orderBy);//load all tasks to table
		$('#action').val("Preview");
		function load_data(isadmin, page, orderBy){
			var action = 'Load';
			$.ajax({
				url: "./Models/Action.php",
				method: "POST",
				data: {action:action, isadmin:isadmin, page:page, orderBy:orderBy},
				success(data){
					$('#task_table').html(data);
				}//success
			});//ajax
		}//load data end

		$('#task_form').on('submit', function(event){//submit new task
			event.preventDefault();
			var employee = $('#employee').val();
			var email = $('#email').val();
			var taskcontent = $('#taskcontent').val();
			var fileName = $('#taskimage').val();
			
			var extension = $('#taskimage').val().split('.').pop().toLowerCase();

			if (extension != '') {//chech for file type
				if (jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1) {
					alert("Invalid image file\nOnly JPG/GIF/PNG formats are allowed ");
					$('#taskimage').val('');
					return false;
				}
			}
			if (employee != '' && email != '' && taskcontent != '') {//chech weather all fields ar filled

				var result = confirm("Preview \nEmployee : "+employee+"\nEmail: "+email+"\nTask: "+taskcontent+"\nImage: "+fileName);
				if (result == true) { 
	                $.ajax({
						url:"./Models/Action.php",
						method:"POST",
						data: new FormData(this),
						contentType:false,
						processData:false,
						success:function(data){
								$('#task_form')[0].reset();
								load_data(isadmin,page,orderBy);
						}//success
					})//ajax 
	            } else { 
	                alert("no"); 
	            } 
			}else{
				alert('All fields are required !');
			}
		});
		
		$('#logout').click(function(){
			var isadmin =0;
			document.getElementById("logout").style.visibility="hidden";
			load_data(isadmin,page,orderBy);
		});

		$('#login').click(function(){
			load_data(isadmin,page,orderBy);
		});

		$(document).on('click', '.update', function(){//update choosen task
			var task_id = $(this).data("id");
			var employee = $('#'+task_id).children('td[data-target=employee]').text();
			var email = $('#'+task_id).children('td[data-target=email]').text();
			var taskcontent = $('#'+task_id).children('td[data-target=taskcontent]').text();
			var taskimage = $('#'+task_id).children('td[data-target=img]').text();

			$('.collapse').collapse("show");
			$('#employee').val(employee);
			$('#email').val(email);
			$('#taskcontent').val(taskcontent);
			$('#button_action').val("Edit");
			$('#action').val("Edit");
			$('#task_id').val(task_id);
			});//update end

		$(document).on('click', '.check-completed', function(){
			var task_id = $(this).data("id");
  			if($(this).prop("checked") == true){
                var value = 1;
            }
            else if($(this).prop("checked") == false){
				var value = 0;
            }
			var action = "Checked";
			$.ajax({
				url:"/Models/Action.php",
				method:"POST",
				data: {ID:task_id, action:action, value:value},
				success:function(data){
						load_data(isadmin,page,orderBy);
				}//success
			})//ajax
		});


	});//document end
</script>

