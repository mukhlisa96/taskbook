<?php
require_once("Crud.php");
	$crud_object = new Crud();

		if (isset($_POST['action'])) {
			if ($_POST['action'] == 'Load') {
				$rowsperpage = 3;
				$page = $_POST["page"];
				if ($page == 0 or $page == '') {
					$page = 1;
				}
				$p = ($page -1)*$rowsperpage;
				$all_rows = $crud_object->execute_query("SELECT * FROM tasks");
				$count = mysqli_num_rows($all_rows);


			if (isset($_POST['orderBy'])) {
				if ($_POST['orderBy'] == 'nameASC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY employee ASC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'nameDESC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY employee DESC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'emailASC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY email ASC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'emailDESC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY email DESC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'statusASC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY completed ASC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'statusDESC') {
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY completed DESC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}elseif ($_POST['orderBy'] == 'ID'){
					echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY ID DESC LIMIT ".$p.",".$rowsperpage."" , $_POST['isadmin']);
				}
			}

			?>
			<form method="POST" align=center>
			<?php
				if ($_POST["page"]>1) {
					$prev_page = $_POST["page"] - 1;
					echo '<button name="page" value="'.$prev_page.'" class="btn btn-default">prev</button>';
				}
				$limit = ceil($count/$rowsperpage);
				for ($i=1; $i < $limit; $i++) { 
					if ($i == $_POST["page"] ) {
						echo '<button name="page" value="'.$i.'" disabled class="btn btn-default"><strong>'.$i.'</strong></button>';
					}else{
						echo '<button name="page" value="'.$i.'" class="btn btn-default">'.$i.'</button>';
					}
				}
				$check = $p + $rowsperpage;
				if ($count>$check) {
					$next_page = $_POST["page"] + 1;
					echo '<button name="page" value="'.$next_page.'" class="btn btn-default">next</button>';
				}
			?>
			</form>
			<?php
		}
		if ($_POST['action'] == 'Preview') {
			echo $_POST['action'];
			$employee = mysqli_real_escape_string($crud_object->connect, $_POST["employee"]);
			$email = mysqli_real_escape_string($crud_object->connect, $_POST["email"]);
			$taskcontent = mysqli_real_escape_string($crud_object->connect, $_POST["taskcontent"]);
			$taskimage = $crud_object->upload_file($_FILES);
			$query = "INSERT INTO tasks	(employee, email, taskcontent, taskimage) VALUES('".$employee."', '".$email."', '".$taskcontent."', '".$taskimage."')";
			$crud_object->execute_query($query);
		}
			if ($_POST['action'] == 'Edit') {
			$employee = mysqli_real_escape_string($crud_object->connect, $_POST["employee"]);
			$email = mysqli_real_escape_string($crud_object->connect, $_POST["email"]);
			$taskcontent = mysqli_real_escape_string($crud_object->connect, $_POST["taskcontent"]);
			$ID = mysqli_real_escape_string($crud_object->connect, $_POST["task_id"]);
			$taskimage = $crud_object->upload_file($_FILES);
			$query = "UPDATE tasks SET employee='".$employee."', email='".$email."', taskcontent ='".$taskcontent."', taskimage ='".$taskimage."' WHERE id='".$_POST["task_id"]."' ";
			$crud_object->execute_query($query);
		}

		if ($_POST['action'] == 'Checked') {
			$query = "UPDATE tasks SET completed='".$_POST["value"]."' WHERE id='".$_POST["ID"]."' ";
			$crud_object->execute_query($query);
			echo $crud_object->get_data_in_table("SELECT * FROM tasks ORDER BY ID DESC", $_POST['isadmin']);
		}
	}//if isset
?>