<?php 
/**
* 
*/
class Database
{
	
	public $connect;
	public $host = "localhost";
	public $username = "root";
	public $password = "";
	public $database = "taskbook";
	public function __construct()
	{
		$this->database_connect();
	}
	public function database_connect(){
		$this->connect = mysqli_connect($this->host,
										$this->username,
										$this->password,
										$this->database);
	}
	public function execute_query($query){
		return mysqli_query($this->connect, $query);
	}
}
?>