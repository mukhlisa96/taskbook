 <?php

 class Route{
 	public static $validRoutes = array();

 	public static function set($route, $function){
 		self::$validRoutes[] = $route;
		$uri = explode('/', rtrim($_SERVER['REQUEST_URI'],'/'));
		if($uri[1]==$route){
			$function->__invoke();
		}
 	}
 }
 ?>