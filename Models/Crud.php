<?php
 require_once 'Database.php';
class Crud extends Database
{
			
	
public function get_data_in_table($query, $isadmin){
		$result = $this->execute_query($query);
		$output = '';
		$output.='
		<table id="user_data" class="table table-bordered table-striped">
					<tr>
						<th width="5%">ID</th>
						<th width="10%">Image</th>
						<th width="10%">
						<form method="POST">Employee
							<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="nameASC"><i class="glyphicon glyphicon-menu-up"></i></button>
            				<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="nameDESC"><i class="glyphicon glyphicon-menu-down"></i></button>
            				</form>
						</th>
						<th width="10%">
						<form method="POST">Email
							<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="emailASC"><i class="glyphicon glyphicon-menu-up"></i></button>
            				<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="emailDESC"><i class="glyphicon glyphicon-menu-down"></i></button>
            				</form>
						</th>
						<th width="10%">Task</th>
						<th width="10%">Update</th>
						<th width="20%">
						<form method="POST">Status
							<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="statusASC"><i class="glyphicon glyphicon-menu-up"></i></button>
            				<button class="btn btn-default arrow" style="border: none" type="submit" name="orderBy" id="action" value="statusDESC"><i class="glyphicon glyphicon-menu-down"></i></button>
            			</form>
						</th>
					</tr>';
		while ($row = mysqli_fetch_object($result)) {
			$output .= '
					<tr id="'.$row->ID.'">
						<td width="5%" data-target="id">'.$row->ID.'</td>
						<td width="10%" data-target="img"><img src="'.$row->taskimage.'" class="img-thumbnail" width="50" height="35"/></td>
						<td width="20%" data-target="employee">'.$row->employee.'</td>
						<td width="20%" data-target="email">'.$row->email.'</td>
						<td width="20%" data-target="taskcontent">'.$row->taskcontent.'</td>
						'.($isadmin==1 ? 
							($row->completed ==0 ? '
							<td width="5%"><button type="button" name="update" data-id="'.$row->ID.'" class="btn btn-success btn-xs update">update</button></td>
							<td width="20%"><input type="checkbox" class="form-check-input check-completed" data-id="'.$row->ID.'" ></td>':'
							<td width="5%"><button type="button" name="update" data-id="'.$row->ID.'" class="btn btn-success btn-xs update">update</button></td>
							<td width="20%"><input type="checkbox" class="form-check-input check-completed" data-id="'.$row->ID.'" checked></td>'
						):($row->completed ==0 ? '
							<td width="10%"><button type="button" name="update" data-id="'.$row->ID.'" class="btn btn-warning btn-xs update" disabled >update</button></td>
							<td width="20%"><input type="checkbox" class="form-check-input check-completed" data-id="'.$row->ID.'"  disabled ></td>':'
							<td width="5%"><button type="button" name="update" data-id="'.$row->ID.'" class="btn btn-warning btn-xs update" disabled >update</button></td>
							<td width="20%"><input type="checkbox" class="form-check-input check-completed" data-id="'.$row->ID.'"  disabled checked></td>')).'
					</tr>';  
		}//while end
		$output.='</table>';
		return $output;
	}//get_data_in_table

	public function upload_file($a){

		function resizeImage($resourceType,$image_width, $image_height){//resizes image upto 320X240 before uploading
			$rWidth = 320;
			$rHeight = 240;
			$imageLayer = imagecreatetruecolor($rWidth, $rHeight);
			imagecopyresampled($imageLayer, $resourceType, 0, 0, 0, 0, $rWidth, $rHeight, $image_width, $image_height);
			return $imageLayer;
		}
			$imageProcess = 0;
		if (is_array($a)) {
			$fileName = $a["taskimage"]['tmp_name'];
			$sourceProperties = getimagesize($fileName);
			$resizeFileName = $a["taskimage"]['name'];
			$uploadPath = "../upload/";
			$fileExt = pathinfo($a["taskimage"]['name'], PATHINFO_EXTENSION);
			$uploadImageType = $sourceProperties[2];
			$sourceImageWidth = $sourceProperties[0];
			$sourceImageHeight = $sourceProperties[1];

			switch ($uploadImageType) {
				case IMAGETYPE_JPEG:
					$resourceType = imagecreatefromjpeg($fileName);
					$imageLayer = resizeImage($resourceType, $sourceImageWidth, $sourceImageHeight);
					imagejpeg($imageLayer, $uploadPath.$resizeFileName);
					break;
				case IMAGETYPE_GIF:
					$resourceType = imagecreatefromgif($fileName);
					$imageLayer = resizeImage($resourceType, $sourceImageWidth, $sourceImageHeight);
					imagegif($imageLayer, $uploadPath.$resizeFileName);
					break;

				case IMAGETYPE_PNG:
					$resourceType = imagecreatefrompng($fileName);
					$imageLayer = resizeImage($resourceType, $sourceImageWidth, $sourceImageHeight);
					imagepng($imageLayer, $uploadPath.$resizeFileName);
					break;

				default:
					$imageProcess = 0;
					break;
			}

			$imageProcess = 1;

			$fileName = $uploadPath.$resizeFileName;
			return $fileName;

		}
		$imageProcess = 0;
	}
}//Crud class end
?>
